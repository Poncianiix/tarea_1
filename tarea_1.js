
// Pregunta 1
// Baja el archivo grades.json y en la terminal ejecuta el siguiente comando: $ mongoimport -d students -c grades < grades.json
// Respuesta: Se importaron 800 documentos

// Pregunta 2
// ¿Cuántos registros arroja el siguiente comando? db.grades.countDocuments()
// Respuesta: 800

// Pregunta 3
//  Encuentra todas las calificaciones del estudiante con el id numero 4.
// Respuesta: 
/*
[
    {
      _id: ObjectId("50906d7fa3c412bb040eb587"),
      student_id: 4,
      type: 'exam',
      score: 87.89071881934647
    },
    {
      _id: ObjectId("50906d7fa3c412bb040eb588"),
      student_id: 4,
      type: 'quiz',
      score: 27.29006335059361
    },
    {
      _id: ObjectId("50906d7fa3c412bb040eb589"),
      student_id: 4,
      type: 'homework',
      score: 5.244452510818443
    },
    {
      _id: ObjectId("50906d7fa3c412bb040eb58a"),
      student_id: 4,
      type: 'homework',
      score: 28.656451042441
    }
  ]
  
*/
// Pregunta 4
// ¿Cuántos registros hay de tipo exam?
// Respuesta: db.grades.find({type: "exam"}).count() = 200

// Pregunta 5
// ¿Cuántos registros hay de tipo homework?
// Respuesta: db.grades.find({type: "homework"}).count() = 400

// Pregunta 6
// ¿Cuántos registros hay de tipo quiz?
// Respuesta: db.grades.find({type: "quiz"}).count() = 200

// Pregunta 7
// Elimina todas las calificaciones del estudiante con el id numero 3
// Respuesta: db.grades.deleteOne({student_id: 3})
// { acknowledged: true, deletedCount: 4 }

// Pregunta 8
// ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?
// Respuesta: db.grades.find({score: 75.29561445722392})
/*
[
    {
      _id: ObjectId("50906d7fa3c412bb040eb59e"),
      student_id: 9,
      type: 'homework',
      score: 75.29561445722392
    }
]
*/

// Pregunta 9
// Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100
// Respuesta: db.grades.updateOne({_id: ObjectId("50906d7fa3c412bb040eb591")}, {$set: {score: 100}})
/*
{
    acknowledged: true,
    insertedId: null,
    matchedCount: 1,
    modifiedCount: 1,
    upsertedCount: 0
}
*/

// Pregunta 10
// A qué estudiante pertenece esta calificación.
// Respuesta: db.grades.find({_id: ObjectId("50906d7fa3c412bb040eb591")})
/*
[
  {
    _id: ObjectId("50906d7fa3c412bb040eb591"),
    student_id: 6,
    type: 'homework',
    score: 100
  }
]
*/

